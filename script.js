const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();
const productRoutes = require("./routes/productRoutes.js");
const userRoutes = require("./routes/userRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const PORT = process.env.PORT || 4000;

mongoose.connect('mongodb+srv://kaisertabuada:2qjhsn9Q@batch139.f3dzn.mongodb.net/ecommerce-api?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', () => console.log("Connected to database..."));


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());
app.use("/api/products", productRoutes);
app.use("/api/users", userRoutes);
app.use("/api/orders", orderRoutes)

app.listen(PORT, () => console.log(`Server running at port ${PORT}...`));