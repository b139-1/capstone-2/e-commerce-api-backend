const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "What are you selling?"]
		}, 
		description: {
			type: String,
			required: [true, "Please provide product description"]
		}, 
		price: {
			type: Number,
			required: [true, "Product's price isn't free, is it?"]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		stock: {
			type: Number,
			default: 0,
			required: [true, "Out of stocks"]
		},
		createdOn: {
			type: Date,
			default: new Date()
		}
	}
);

module.exports = mongoose.model("Product", productSchema);