const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		},
		totalAmount: {
			type: Number,
			default: 0
		},
		purchasedOn: {
			type: Date, 
			default: new Date()
		},
		items: [
			{
				productId: {
					type: String,
					required: [true, "Product ID is required"]
				},
				price: {
					type: Number,
					default: 0,
					required: [true, "Price is required"]
				},
				quantity: {
					type: Number,
					default: 0
				},
				orderedOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
);

module.exports = mongoose.model("Order", orderSchema);