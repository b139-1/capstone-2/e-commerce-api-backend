const auth = require("./../auth.js")
const User = require("./../models/User.js");
const Product = require("./../models/Product.js");
const Order = require("./../models/Order.js");

module.exports.getAllOrders = (data) => {
	return User.findById(data).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return Order.find().then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}

module.exports.checkout = (data) => {
	const {userId, productId, quantity} = data

	return User.findById(userId).then((result, err) => {
		if(err) {
			return false
		} else {
			return Product.findById(productId).then((result, err) => {
				let newOrder = new Order({
					userId: userId,
					totalAmount: quantity * result.price,
					items: [{
						productId: productId,
						price: result.price,
						quantity: quantity
					}]
				})
				return newOrder.save().then((result, err) => {
					if(err) {
						return false
					} else { 
						return true 
					}
				})
			})
		}
	})
}

module.exports.getMyOrders = (data) => {
	return User.findById(data).then((result, err) => {
		if(err) {
			return false
		} else {
			return Order.find().then((result, err) => {
				if(err) {
					return false 
				} else {
					return result
				}
			})
		}
	})
}

module.exports.deleteOrder = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(err) {
			return false
		} else {
			return Order.findByIdAndDelete(data).then((result, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		}
	})
}