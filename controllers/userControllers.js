const auth = require("./../auth.js")
const User = require("./../models/User.js");
const Product = require("./../models/Product.js");
const Order = require("./../models/Order.js");
const bcrypt = require("bcrypt");


module.exports.checkEmail = (data) => {
	const {email} = data
	return User.findOne({email: email}).then((result, err) => {
		if(result != null) {
			return `Email already exists!`;
		} else {
			if(err) {
				return false;
			} else {
				return true;
			}
		}
	})
}

module.exports.register = (data) => {
	let newUser = new User({
		email: data.email,
		password: bcrypt.hashSync(data.password, 10)
	});

	return User.findOne({email: data.email}).then((result, err) => {
		if(result !== null) {
			return `Email already exists!`
		} else {
			return newUser.save().then((result, err) => {
				if(err) {
					return false;
				} else {
					return true;
				}
			});
		}
	})
}

module.exports.getAllUsers = (data) => {
	return User.findById(data).then((result, error) => {
		if(result.isAdmin == false) {
			return `Sorry, you need admin permissions to access this request`
		} else {
			return User.find().then((result, err) => {
				if(err) {
					return false
				} else {
					result.password = "";
					return result;
				}
			})
		}
	});
}

module.exports.login = (data) => {
	const {email, password} = data;
	return User.findOne({email: email}).then((result, err) => {
		if(result == null) {
			return false;
		} else {
			let isSamePassword = bcrypt.compareSync(password, result.password);
			if(isSamePassword == true) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			}
		}
	});
}

module.exports.getProfile = (data) => {
	return User.findById(data).then((result, err) => {
		if(result == null){
			return false;
		} else {
			result.password = "";
			return result
		}
	})
}

module.exports.makeAdmin = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return User.findByIdAndUpdate(data, {isAdmin: true}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}
