const auth = require("./../auth.js")
const User = require("./../models/User.js");
const Product = require("./../models/Product.js");
const Order = require("./../models/Order.js");

module.exports.getAllProducts = () => {
	return Product.find({isActive: true}).then((result, err) => {
		if(result) {
			return result
		} else {
			return false
		}
	})
}

module.exports.findProduct = (data) => {
	return Product.findById(data).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}

module.exports.addProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			let newProduct = new Product({
				name: data.body.name,
				description: data.body.description,
				price: data.body.price,
				stock: data.body.stock
			});

			return newProduct.save().then((result, err) => {
				if(err) {
					return false
				} else {
					return true
				}
			})
		}
	})
}

module.exports.updateProduct = (admin, id, data) => {
	return User.findById(admin).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			const {name, price, stock} = data
			const update = {
				name: name,
				price: price,
				stock: stock
			}
			
			return Product.findByIdAndUpdate(id, update, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}

module.exports.archiveProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return Product.findByIdAndUpdate(data, {isActive: false}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}

module.exports.unarchiveProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return Product.findByIdAndUpdate(data, {isActive: true}, {new: true}).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}

module.exports.deleteProduct = (id, data) => {
	return User.findById(id).then((result, err) => {
		if(result.isAdmin == false) {
			return "Sorry, you need admin permissions to access this request"
		} else {
			return Product.findByIdAndDelete(data).then((result, err) => {
				if(err) {
					return false
				} else {
					return result
				}
			})
		}
	})
}