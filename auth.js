const jwt = require("jsonwebtoken");
const secret = "eCommerceAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	if(token == null){
		return null
	}
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, result) => {
			if(err) {
				return res.send(false); 
			} else {
				return next();
			}
		})
	}
}

module.exports.decode = (token) => {
	if(typeof token != "undefined") {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, result) => {
			if(err) {
				return null;
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
}