const express = require("express");
const router = express.Router();
const orderController = require("./../controllers/orderControllers.js");
const auth = require("./../auth.js");

let verifiedId;

router.get("/", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.getAllOrders(verifiedId).then(result => res.send(result))
})

router.post("/checkout", auth.verify, (req, res) => {
	let orders = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}
	orderController.checkout(orders).then(result => res.send(result))
})

router.get("/my-orders", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.getMyOrders(verifiedId).then(result => res.send(result))
})

router.delete("/:id/delete-order", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	orderController.deleteOrder(verifiedId, req.params.id).then(result => res.send(result))
})

module.exports = router;