const express = require("express");
const router = express.Router();
const userController = require("./../controllers/userControllers.js");
const auth = require("./../auth.js");

let userData;

router.post("/check-user", (req, res) => {
	userController.checkEmail(req.body).then(result => res.send(result))
})

router.post("/register", (req, res) => {
	userController.register(req.body).then(result => res.send(result));
});

router.get("/", auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization).id
	userController.getAllUsers(userData).then(result => res.send(result));
});

router.post("/login", (req, res) => {
	userController.login(req.body).then(result => res.send(result));
});

router.get("/profile", auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization).id
	userController.getProfile(userData).then(result => res.send(result));
});

router.put("/:id/set-as-admin", auth.verify, (req, res) => {
	userData = auth.decode(req.headers.authorization).id
	userController.makeAdmin(userData, req.params.id).then(result => res.send(result));
});


module.exports = router;