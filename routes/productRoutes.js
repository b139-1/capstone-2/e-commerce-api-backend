const express = require("express");
const router = express.Router();
const productController = require("./../controllers/productControllers.js");
const auth = require("./../auth.js")

let verifiedId;

router.get("/", (req, res) => {
	productController.getAllProducts().then(result => res.send(result))
});

router.get("/:id", (req, res) => {
	productController.findProduct(req.params.id).then(result => res.send(result))
});

router.post("/add-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.addProduct(verifiedId, req).then(result => res.send(result))
});

router.put("/:id/update-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.updateProduct(verifiedId, req.params.id, req.body).then(result => res.send(result))
});

router.put("/:id/archive-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.archiveProduct(verifiedId, req.params.id).then(result => res.send(result))
});

router.put("/:id/unarchive-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.unarchiveProduct(verifiedId, req.params.id).then(result => res.send(result))
});

router.delete("/:id/delete-product", auth.verify, (req, res) => {
	verifiedId = auth.decode(req.headers.authorization).id;
	productController.deleteProduct(verifiedId, req.params.id).then(result => res.send(result))
})

module.exports = router;